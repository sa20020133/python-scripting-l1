data = []
url = ["www.annauniv.edu", "www.google.com", "www.ndtv.com", "www.website.org", "www.bis.org.in", "www.rbi.org.in"]

for i in url:
    d = i.strip().split(".")
    d.reverse()
    data.append(d)

data.sort()

sorted_data = []

for i in data:
    i.reverse()
    sorted_data.append(".".join(i))
print(sorted_data)
